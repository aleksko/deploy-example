#!/bin/bash

[[ ! -f .env ]] && echo -e "\e[31m .env file is not isset, pls add this file from .env.example \e[0m" &&  exit 0;

# Load up .env
set -o allexport

    [[ -f .env ]] && source .env

set +o allexport

set +e

cd /var/www/app;

#git pulling

echo -e "\e[42m git pulling ... Branch: ${BRANCH} \e[0m";

[[ -z ${BRANCH} ]] && echo -e "\e[31m no branch \e[0m" && exit 1;

git remote rm origin;
git remote add origin "${REMOTE_REPOSITORY}";

if [ ! -n "$(grep "gitlab.com " ~/.ssh/known_hosts)" ]; then ssh-keyscan "gitlab.com" >> ~/.ssh/known_hosts 2>/dev/null; fi

git config --global user.email "deploy@mail.com"
git config --global user.name "deployer"
git config --global core.fileMode false;

git fetch origin ${BRANCH} || exit 1;
#git reset --hard "origin/${BRANCH}" || exit 1;
git pull origin "${BRANCH}" || exit 1;
git submodule update --init --recursive --remote --force || exit 1;

#composer

echo -e "\e[42m composer install ... \e[0m";

composer install || exit 1;

#migrations

echo -e "\e[42m migrations up ... \e[0m";

php ./local/tools/migrate.php up || exit 1;

#clearing cache

echo -e "\e[42m clearing cache \e[0m";

php ./local/tools/clear_cache.php cron || exit 1;

#submodules update

echo -e "\e[42m submodules update ... \e[0m";

for D in $(find "./submodules" -mindepth 1 -maxdepth 1 -type d) ; do

   echo -e "Start submodule update ---> \e[5m ${D} \e[25m";

   npm --prefix ${D} install;

   if [ -z ${NPM_BUILD_TYPE} ];

     then
       npm --prefix ${D} run build;
     else
       npm --prefix ${D} run build:${NPM_BUILD_TYPE};

   fi

done